import { validator, builValidations } from 'ember-cp-validations';

export default builValidations({

    num: [

        validator('number', {
            allowString: true,
            integer: true,
            message: 'Error! es no es un numero'
        }),
        validator('presence', true)
    ],
    email: [
        validator('format', {
            type: 'email',
            message: 'Error! es no es una direccion de correo'
        }),
    ],
});