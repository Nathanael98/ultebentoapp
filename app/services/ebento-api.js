import Service from '@ember/service';
import { computed } from '@ember/object';
import fetch from 'fetch';

export default Service.extend({
	token: null,
	eventId: null,
	initialized: false,
	host: "https://events.ebento.xyz/api/v1/public/",
	headers: computed('token', 'eventId', function() {
		if (!this.get('initialized'))
			return {}
		else return {
				"Authorization": `Token token="${this.get('token')}", e="${this.get('eventId')}"`
			}

	}),
	initialize(token, eventId) {
		return new Promise((res) => {
			this.set('token', token)
			this.set('eventId', eventId)
			this.set('initialized', true)
			res()
		})

	},

	request(model) {
		console.log(`${this.get('host')+model}`)
		return fetch(`${this.get('host')}${model}`, {
			method: 'get',
			headers: this.get('headers')
		}).then(function(response) {			
			return response.json();			
		});

	}
});
