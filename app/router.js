import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('login');
  this.route('logged', { path: '/' }, function() {
    this.route('actividades');
    this.route('detalleactividades');
    this.route('menuactividades');
    this.route('ponentes');
    this.route('detalleponentes');
    this.route('home');
    this.route('contactos');
    this.route('ajustes');
    this.route('ajustescuenta');
    this.route('actvacias');
  });
  this.route('chat');
  this.route('prueba');
  this.route('example');
  this.route('registrar');
  this.route('api-test');
});

export default Router;
