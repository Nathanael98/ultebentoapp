import DS from 'ember-data';

export default DS.Model.extend({

    
    name: DS.attr('string'),
    //tokens: DS.hasMany('token'),
    account: DS.hasMany('account'),

});
