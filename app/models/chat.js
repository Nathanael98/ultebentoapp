import DS from 'ember-data';

export default DS.Model.extend({

    sender: DS.attr('string'),
    message: DS.attr('string'),
    destinnation: DS.attr('string'),
    hora: DS.attr('string'),

});
