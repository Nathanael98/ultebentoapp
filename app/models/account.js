import DS from 'ember-data';

export default DS.Model.extend({

    name: DS.attr('string'),
    img: DS.attr('string'),
    interes: DS.attr('string'),
    eventos: DS.hasMany('evento'),
    //tokens: DS.hasMany('token'),

});
