import DS from 'ember-data';


export default DS.Model.extend({

    description: DS.attr('string'),
    email: DS.attr('string'),
    ids: DS.attr('string'),
    img: DS.attr('string'),
    job: DS.attr('string'),
    name: DS.attr('string'),
    social: DS.attr('string'),    
    
});