import Route from '@ember/routing/route';

export default Route.extend({
    /*  model(){
         return this.store.findAll('persona',{});
     }    */

    model() {
        /* return this.store.findAll('persona',{});  */

        return [
            ["INAUGURACIÓN", "vie. 18 de may. de 2018", "4:00 pm - 4:15 pm", "Natividad Castrejón Valdez", "Presentación", "18 Mayo"],
            ["SER Y SENTIDO: ANÁLISIS DEL SER INTERROGADO", "vie. 18 de may. de 2018", "4:15 pm - 5:15 pm", "Juan Felipe Miramontes Hernández", "Conferencia"],
            ["LOS MITOS DE LA RESILIENCIA", "vie. 18 de may. de 2018", "5:15 pm - 6:15 pm", "Patricia Marcela Crast", "Conferencia"],
            ["RECESO", "vie. 18 de may. de 2018", "6:15 pm - 6:45 pm", "", "Descanso"],
            ["CASO DE EXITO", "vie. 18 de may. de 2018", "6:45 pm - 7:15 pm", "", "Conferencia"],
            ["EL COMPORTAMIENTO NO VERBAL: UNA HERRAMIENTA PARA INCORPORAR EN LA NUEVA CURRICULA", "vie. 18 de may. de 2018", "7:15 pm - 8:15 pm", "José Luis Cañavate Toribio", "Conferencia"],
        ]
    }


});
