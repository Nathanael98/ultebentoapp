import Route from '@ember/routing/route';
import RSVP from 'rsvp';


export default Route.extend({

    model() {
        /* return this.store.findAll('persona', {}); */

        /* return RSVP({
            cargar: this.store.query('persona', {

                orderBy: 'name',
                equalTo: 'Arturo Cisneros Cuayahuitl'
            }) .then((datos) => {
                console.log(datos)
                datos.forEach(element => {
                    console.log("Entro aqui")
                    console.log(element.img);
                }) 
            }) 

        })*/


        return RSVP.hash({
            cargar: this.store.query('account', {
                orderBy: 'name',
                equalTo: ''
            }) .then((datos) => {
                console.log(datos)
                datos.forEach(element => {
                    console.log("Entro aqui")
                    console.log(element.img);
                }) 
            }),

            cargarCuenta: this.store.findAll('account', {}),

            cargarEvento: this.store.findAll('evento', {}),

        })
    }

});
