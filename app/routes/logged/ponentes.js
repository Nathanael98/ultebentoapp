import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({

    
        //return this.store.findAll('persona',{});

        model() {
            //return this.store.findAll('persona', {});
    
            return RSVP.hash({

                cargarTodo: this.store.findAll('persona'),
                
                cargar: this.store.query('persona', {
    
                    orderBy: 'name',
                    equalTo: 'Arturo Cisneros Cuayahuitl'
                
                })
            })
        }
    

});
