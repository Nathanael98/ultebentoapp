import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({

    model() {
        //return this.store.findAll('token', {});
         return RSVP.hash({
            //cargarTodo: this.store.findAll('persona'),            
            userEvento: this.store.query('account', {
                orderBy: 'name',
                equalTo: 'Fiesta'            
            }),

            cargarEvento: this.store.findAll('evento', {}),            

            cargarUsuarios: this.store.query('account', {

                orderBy: 'name',
                equalTo: (!'Natha'),                                 
            
            }),

            cargarUsuario: this.store.query('account' , { name: ['Natha' ,"JJ"]}),
        }) 
    },
    

});
