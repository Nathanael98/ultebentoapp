import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { validator, buildValidations } from 'ember-cp-validations';

const Validations = buildValidations({

    'token': [
        validator('presence', {
            presence: true,
            message: 'El campo no puede estar vacio'
        }),
        validator('length', {
            max: 6,
            min: 6,
            message: 'Debe ingresar 6 digitos'
        })

    ],
});

export default Controller.extend(Validations, {
    session: service(),

    actions: {

        toogleError(attr) {
            switch (attr) {
                case 'token':
                    this.set('tokenErrorCheck', true)
                    break
            }
        },

        login(token) {
        
            if (token){
                this.transitionToRoute('logged.home')       
            }else{ 
                alert("Asegurese de llenar todos los campos")                                                                   
            }
        }
    }
});
