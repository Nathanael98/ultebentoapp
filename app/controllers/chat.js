import Controller from '@ember/controller';
import { computed } from '@ember/object';
import { validator, buildValidations } from "ember-cp-validations";


const validations = buildValidations({
    'textMensaje':
        validator('presence', {
            presence: true,
            message: 'Escribe texto primero'
        })

})

export default Controller.extend(validations, {

    queryParams: ['message'],
    message: 'fghfgh',


    /* enviarDatos: computed('name', 'model', function () {
        let name = this.get('name');
        nombre = this.get('model');

        if (name) {
            return nombre.filterBy('name', name);
        } else {
            return nombre;
        }
    }), */


    actions: {

        toggleError(attr) {
            switch (attr) {
                case 'textMensaje':
                    this.set('txtAreaErrorCheck', true)
                    break
            }
        },
        createMensaje() {

            let hoy = new Date();
            let fecha = hoy.getDate() + '-' + (hoy.getMonth() + 1) + '-' + (hoy.getFullYear());
            let horas = hoy.getHours() + ":" + hoy.getMinutes();
            let dateAndHour = fecha + " " + horas;


            this.validate().then(({ validations }) => {
                if (validations.get('isValid')) {

                    let mensaje = this.store.createRecord('chat', {
                        message: this.textMensaje,
                        hora: dateAndHour,
                        sender: "Natha",                        
                    })
                    mensaje.save().then(() => {
                        this.set('textMensaje', '')

                    })

                }

            }
            )


        },

        auto(element) {            
            element.style.height = (element.scrollHeight) + "px";
        },

        auto_grow(element) {
            element.style.height = (element.scrollHeight) + "px";
        }


    }
});