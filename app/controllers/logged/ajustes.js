import Controller from '@ember/controller';
import { validator, buildValidations } from 'ember-cp-validations';

const Validations = buildValidations({
    'token': [
        validator('presence', {
            presence: true,
            message: 'El campo no puede estar vacio'
        }),
    ]
});


export default Controller.extend(Validations, {

    actions: {
        logout() {
            swal({
                title: '¿Esta seguro de cerrar sesión?',
                text: 'Esta a punto de abandonar la APP',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {

                /* if (result.value) {
                    this.get('session').close().then(() => {
                    this.transitionToRoute('login')
                    })
                } */
            });
        },

        toogleError(attr) {
            switch (attr) {
                case 'token':
                    this.set('tokenErrorCheck', true)
                    break
            }
        },

        newEvento(token) {
        },

    }
});
