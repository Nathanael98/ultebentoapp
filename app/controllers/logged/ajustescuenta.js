import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { validator, buildValidations } from 'ember-cp-validations';

/* Validaciones... */
const Validations = buildValidations({


    'interes': [
        validator('presence', {
            presence: true,
            message: 'El campo no puede estar vacio'
        }),
        validator('length', {
            max: 30,
            message: 'No debe de excederce a mas de 30 caracteres'
        })
    ],


});


export default Controller.extend(Validations, {

    actions: {

        select(post) {
            console.log(post.get('title'));
        },

        toogleError(attr) {
            switch (attr) {
                case 'interes':
                    this.set('interesErrorCheck', true)
                    break
            }
        },

        actualizarPerfil(perfil) {

            perfil.save()

        }

    }
});
