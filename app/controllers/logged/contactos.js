import Controller from '@ember/controller';

export default Controller.extend({    

    actions: {
        crearToken() {
            let codtoken = this.store.createRecord('token', {
                codigo_token: this.token,
            })
            codtoken.save()
        },

        updateToken(codtoken) {
            codtoken.save()
        },


        agregarCuenta(token) {
            token.get('account').createRecord()
        },

        crearUsuario(token, account) {
            account.save().then(() => {
                token.save()
            })
        },       

    }
});
