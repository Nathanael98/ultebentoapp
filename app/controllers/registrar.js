import Controller from '@ember/controller';


export default Controller.extend({
    actions: {
        registrarPersona() {
            var person = this.store.createRecord('persona', {

                description: this.descrip,
                email: this.correo,
                ids: this.codigo,
                img: this.imagen,
                job: this.ocupacion,
                name: this.nombre,
                social: this.redsocial,
            });
            person.save();
        },

        registrarAccount(){

            var cuenta = this.store.createRecord('account', {

                name: this.nombre,                      

            });
            cuenta.save();

        },

        registrarEvento() {
            var evento = this.store.createRecord('evento', {

                name: this.nombre,

            });
            evento.save();
        },


        agregarAccount(event) {
            event.get('account').createRecord()
        },

        crearAccount(evento, account) {

        
            account.save().then(() => {
                evento.save()
            })
        },



        // registrarAccount() {
        //     let valorUser;
        //     let valorCuenta;
        //     var cuenta = this.store.createRecord('account', {
        //         name: this.account,
        //     }).then((datos) => {
        //         datos.save();
        //         valorUser = datos.id;

        //         this.store.createRecord('token', {
        //             codigo_token: "123456asd",
        //             account: "-LHpa8u2GmQLhzwE_a8l",
        //             eventos: "-LHp_Tjimazw_oklIgj3",
        //             idUser: valorUser
        //         }).then((datos2) => {
        //             datos2.save();
        //             valorCuenta = datos2.id
        //             // datos2.forEach(element => {
        //             //     valorCuenta=element.id
        //             // });
        //             var dat;
        //             return dat = this.store.createRecord("eventos", {
        //                 name: "alvercas",
        //                 numEvent: 0,
        //                 idCuenta: valorCuenta
        //             }).then((resultado) => {
        //                 dat.save().then((dd) => {
        //                     console.log("bien " + resultado);
        //                 });

        //             })
        //         })
        //     })
        // },
        // agreagarEventos() {
        //     let cun1;
        //     let valorCuenta;
        //     var newEvent = this.store.query("account", {
        //         orderBy: "name",
        //         equalTo: "natita de sacarias"
        //     }).then((consul1) => {
        //         consul1.forEach(element => {
        //             cun1 = element.idCuenta;
        //         })
        //         this.store.query("token", {
        //             orderBy: "idUser",
        //             equalTo: cun1
        //         }).then((consul2) => {
        //             consul2.forEach(elemen => {
        //                 valorCuenta = elemen.id;
        //             });
        //             return this.store.createRecord("eventos", {
        //                 name: "alvercas",
        //                 numEvent: 0,
        //                 idCuenta: valorCuenta
        //             }).then((resultado) => {
        //                 resultado.save().then((re) => {
        //                     console.log("bien " + re);
        //                     let red = "change";
        //                 })

        //             })
        //         })
        //     })
        // },

        registrarToken() {

            var cuenta = this.store.createRecord('token', {
                codigo_token: "123456asd",
                account: "-LHpa8u2GmQLhzwE_a8l",
                eventos: "-LHp_Tjimazw_oklIgj3"

            });
            cuenta.save();
        }
    }
});
