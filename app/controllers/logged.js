import Controller from '@ember/controller';

export default Controller.extend({

    actions: {
        logout() {
            swal({
                title: '¿Esta seguro de cerrar sesión?',
                text: 'Esta a punto de abandonar la APP',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {

                if (result.value) {
                    /* this.get('session').close().then(() => { */
                    this.transitionToRoute('login')
                   /*  }) */
                }
            });
        }
    }
});
